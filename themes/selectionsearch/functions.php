<?php

include 'includes/wp_bootstrap_navwalker.php';

function selectionSearch_scripts() {


    wp_enqueue_style('wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600', false);

    wp_enqueue_style('fonts', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style('SSOwl-carousal', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), '1.0.0', true);
    wp_enqueue_style('SSxOwl-carousal', get_template_directory_uri() . '/css/owl.theme.default.min.css', array(), '1.0.0', true);
    wp_enqueue_style('bootstrap-styles', get_template_directory_uri() . '/css/bootstrap.min.css', false, '1.0.0', 'all');

    //wp_enqueue_script('all-js', get_template_directory_uri() . '/js/all.min.js', array('jquery'), '1.0.0', true);

    wp_enqueue_style('style', get_stylesheet_uri(), array(), '1.2');

    //wp_enqueue_script('jquery');

    wp_enqueue_script('selectionSearch_js', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', true);
    wp_enqueue_script('SelectionSearch-carousal', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '1.0.0', true);
    //wp_enqueue_script('SelectionSearch-insert', get_template_directory_uri() . '/js/insert.js', array(), '1.0.0', true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.3.2');
}

add_action('wp_enqueue_scripts', 'selectionSearch_scripts');


add_filter('wpcf7_validate_configuration', '__return_false');

add_theme_support('post-thumbnails');
add_theme_support('title-tag');

function primary_menu() {

    register_nav_menus(array(
        'primary' => __('Primary Menu', 'Top Navigation')
    ));
}

add_action('init', 'primary_menu');



add_action('init', 'create_slider');

function create_slider() {

    $slider_args = array(
        'labels' => array(
            'name' => __('Sliders'),
            'singular_name' => __('Sliders'),
            'add_new' => __('Add New Slider'),
            'add_new_item' => __('Add New Slider'),
            'edit_item' => __('Edit Slider'),
            'new_item' => __('Add New Slider'),
            'view_item' => __('View Slider'),
            'search_items' => __('Search Slider'),
            'not_found' => __('No slider found'),
            'not_found_in_trash' => __('No slider found in trash')
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        'menu_position' => 20,
        'supports' => array('title', 'editor', 'thumbnail')
    );

    register_post_type('slider', $slider_args);
}

add_action('init', 'featured_jobs');

function featured_jobs() {

    $job_args = array(
        'labels' => array(
            'name' => __('Jobs'),
            'singular_name' => __('Jobs'),
            'add_new' => __('Add New Job'),
            'add_new_item' => __('Add New Job'),
            'edit_item' => __('Edit Job'),
            'new_item' => __('Add New Job'),
            'view_item' => __('View Job'),
            'search_items' => __('Search Job'),
            'not_found' => __('No job found'),
            'not_found_in_trash' => __('No job found in trash')
        ),
        'taxonomies' => array('category'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        'menu_position' => 20,
        'supports' => array('title', 'editor', 'thumbnail')
    );

    register_post_type('jobs', $job_args);
}

add_action('init', 'testimonials');

function testimonials() {

    $testimonial_args = array(
        'labels' => array(
            'name' => __('Testimonials'),
            'singular_name' => __('Testimonials'),
            'add_new' => __('Add New'),
            'add_new_item' => __('Add New Testimonial'),
            'edit_item' => __('Edit Testimonial'),
            'new_item' => __('Add New Testimonial'),
            'view_item' => __('View Testimonial'),
            'search_items' => __('Search Testimonial'),
            'not_found' => __('No testimonial found'),
            'not_found_in_trash' => __('No testimonial found in trash')
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        'menu_position' => 20,
        'supports' => array('title', 'editor', 'thumbnail')
    );

    register_post_type('testimonials', $testimonial_args);
}

/* if(function_exists('register_sidebar')){

  register_sidebar(array(
  'name' => 'Right Sidebar',
  'before_title' => '<h2>',
  'after_title' => '</h2>'

  ));
  } */

add_action('wp_ajax_insert_applications', 'insert_applications');
add_action('wp_ajax_nopriv_insert_applications', 'insert_applications');

function insert_applications() {
    global $wpdb;
    $target_path = "http://selectionsearch.in/resume_uploads/";
    $target_path = $target_path . basename($_FILES['resume']['name']);
    move_uploaded_file($_FILES['resume']['tmp_name'], $target_path);        //echo $_FILES['resume']['tmp_name'];
    $first_name = trim($_POST['first_name']);
    $mid_name = trim($_POST['mid_name']);
    $last_name = trim($_POST['last_name']);
    $gender_male = trim($_POST['gender']);

    $post_applied = $_POST['post_applied'];
    $dob = trim($_POST['dob']);
    $father_name = trim($_POST['father_name']);
    $rec_org = trim($_POST['rec_org']);
    $rec_desig = trim($_POST['rec_desig']);
    $mother_name = trim($_POST['mother_name']);
    $mother_emp = trim($_POST['mother_emp']);
    $mother_desig = trim($_POST['mother_desig']);
    $mother_income = trim($_POST['mother_income']);
    $status = trim($_POST['status']);
    $spouse_name = trim($_POST['spouse_name']);
    $spouse_emp = trim($_POST['spouse_emp']);
    $spouse_desig = trim($_POST['spouse_desig']);
    $spouse_income = trim($_POST['spouse_income']);

    $name_children = $_POST['name_children'];
    $sex = $_POST['sex'];
    $age_children = $_POST['age_children'];
    $ed_children = $_POST['ed_children'];
    $school = $_POST['school'];

    $res_add = trim($_POST['res_add']);
    $res_add = htmlspecialchars($_POST['res_add']);
    $comm_add = trim($_POST['comm_add']);
    $comm_add = htmlspecialchars($_POST['comm_add']);
    $tel = trim($_POST['tel']);
    $email = trim($_POST['email']);
    $mother_tongue = trim($_POST['mother_tongue']);
    $other_lang = trim($_POST['other_lang']);

    $exam = $_POST['exam'];
    $institute = $_POST['institute'];
    $subjects = $_POST['subjects'];
    $from_date = $_POST['from_date'];
    $board = $_POST['board'];
    $to_date = $_POST['to_date'];
    $marks = $_POST['marks'];

    $achievements = trim($_POST['achievements']);

    $total_exp = trim($_POST['total_exp']);

    $comp = trim($_POST['comp']);
    $exp_letter = trim($_POST['exp_letter']);
    $hobby = trim($_POST['hobby']);
    $sp_skill = trim($_POST['sp_skill']);

    $name_location = $_POST['name_location'];
    $profile = $_POST['profile'];
    $salary = $_POST['salary'];
    $from_duration = $_POST['from_duration'];
    $board_exp = $_POST['name_board'];
    $reason = $_POST['reason'];
    $upto = $_POST['upto'];
    $subject_class = $_POST['subject_class'];

    $class = $_POST['class'];
    $subject = $_POST['subject'];
    $workshop = trim($_POST['workshop']);
    $teacher_train = trim($_POST['teacher_train']);


    $name_ref = $_POST['name_ref'];
    $occ_ref = $_POST['occ_ref'];
    $add_ref = $_POST['add_ref'];

    //echo $first_name;

    $args = array(
        'first_name' => $first_name,
        'mid_name' => $mid_name,
        'last_name' => $last_name,
        'gender' => $gender_male,
        'post_applied' => $post_applied,
        'dob' => $dob,
        'father_name' => $father_name,
        'resume' => $target_path,
        'recent_organization' => $rec_org,
        'recent_designation' => $rec_desig,
        'mother_name' => $mother_name,
        'mother_employment_organization' => $mother_emp,
        'mother_designation' => $mother_desig,
        'mother_income' => $mother_income,
        'marital_status' => $status,
        'spouse_name' => $spouse_name,
        'spouse_employment_organization' => $spouse_emp,
        'spouse_designation' => $spouse_desig,
        'spouse_income' => $spouse_income,
        'permanent_address' => $res_add,
        'address_communication' => $comm_add,
        'telephone' => $tel,
        'email' => $email,
        'mother_tongue' => $mother_tongue,
        'other_languages' => $other_lang,
        'fine_arts' => $achievements,
        'computer_knowledge' => $comp,
        'experience_of_writing_for_school_newsletter' => $exp_letter,
        'hobby' => $hobby,
        'special_skills' => $sp_skill,
        'total_experience' => $total_exp,
        'workshops' => $workshop,
        'conducted_teacher_training_workshops' => $teacher_train,
    );




    $result = $wpdb->insert('wp_applications', $args);
    //echo $add_ref;
    //$last_id = $wpdb->insert_id();
    //print_r($_FILES['resume']);

    $last_id = $wpdb->insert_id;

    $children_name = implode(",", $name_children);
    $children_sex = implode(",", $sex);
    $children_age = implode(",", $age_children);
    $children_ed = implode(",", $ed_children);
    $children_school = implode(",", $school);

    $children = array(
        'user_id' => $last_id,
        'name' => $children_name,
        'sex' => $children_sex,
        'age' => $children_age,
        'education' => $children_ed,
        'school_or_college' => $children_school
    );

    $result_children = $wpdb->insert('wp_children', $children);

    $exam_name = implode(",", $exam);
    $exam_institute = implode(",", $institute);
    $exam_subjects = implode(",", $subjects);
    $exam_from = implode(",", $from_date);
    $exam_to = implode(",", $to_date);
    $exam_board = implode(",", $board);
    $exam_marks = implode(",", $marks);

    $qualification = array(
        'user_id' => $last_id,
        'exam' => $exam_name,
        'from_date' => $exam_from,
        'to_date' => $exam_to,
        'institute' => $exam_institute,
        'board' => $exam_board,
        'marks' => $exam_marks,
        'subjects' => $exam_subjects
    );

    $result_qualification = $wpdb->insert('wp_qualifications', $qualification);

    $exp_name = implode(",", $name_location);
    $exp_profile = implode(",", $profile);
    $exp_salary = implode(",", $salary);
    $exp_from = implode(",", $from_duration);
    $exp_upto = implode(",", $upto);
    $exp_board = implode(",", $board_exp);
    $exp_reason = implode(",", $reason);
    $exp_class = implode(",", $subject_class);

    $experience = array(
        'user_id' => $last_id,
        'name_location' => $exp_name,
        'from_duration' => $exp_from,
        'upto_duration' => $exp_upto,
        'profile' => $exp_profile,
        'board' => $exp_board,
        'class_and_subject' => $exp_class,
        'salary' => $exp_salary,
        'reason' => $exp_reason
    );

    $result_experience = $wpdb->insert('wp_experience', $experience);

    $name_subject = implode(",", $subject);
    $name_class = implode(",", $class);

    $sub = array(
        'user_id' => $last_id,
        'class' => $name_class,
        'subject' => $name_subject
    );

    $result_sub = $wpdb->insert('wp_subjects', $sub);
}

add_filter('wp_mail_content_type', 'set_content_type');

function set_content_type($content_type) {
    return 'text/html';
}

add_action('wp_ajax_insert_register_applications', 'insert_register_applications');
add_action('wp_ajax_nopriv_insert_register_applications', 'insert_register_applications');

function insert_register_applications() {
    global $wpdb;
   

   print_r($result = $wpdb->get_results("SELECT * FROM wp_register_db WHERE status = 0"));

    foreach ($result as $key => $value) {
                
        $fname = $value->first_name;
//        $mname = $value->mid_name;
//        $lname = $value->last_name;
//        $tel = $value->telephone;
        $mail = $value->email;
        $token = sha1(mt_rand(1, 90000) . 'SALT');
        

//       $args = array(
//            
//            'first_name' => $fname,
//            'mid_name' => $mname,
//            'last_name' => $lname,
//            'telephone' => $tel,
//            'email' => $mail,
//            'token' => $token
//        );
//   
//      $result_insert = $wpdb->insert('wp_register_applications', $args);
//    
        
      //if($result_insert == TRUE){
           
            $headers[] .= 'MIME-Version: 1.0\r\n' .
                    'Content-Type: text/html; charset=UTF-8\r\n';
            $headers[] .= 'From:' . "Selection Search <anuradha@selectionsearch.in> \r\n";
            
            $message .= 'Dear&nbsp;' . $fname . '<br><br>';

            $message .= 'Greetings from Selection Search ! <br><br>

Thank you for your support and trust in our services and we hope to improve our services with every passing day.  To help you in your career path and recommend better opportunities, we have developed a form which will give us a complete picture of your candidature.  The form has 37 fields and each one is important.  This has been devised based on solid research and experience of what is the information the client/school is seeking for.<br><br>

We would appreciate if you could <a href="http://selectionsearch.in/register-email?key=' . $token . '"> Click here! </a>
and fill in the application.<br><br>

This will help us to get in touch with you as soon as we get the right opportunity.<br><br>

Thanks<br>
Anuradha Dravid<br>
CEO<br>
Selection Search<br>
Delhi/NCR<br>
Mobile 07291987087';




            $email_status = wp_mail($mail, 'Confirmation mail', $message, $headers);
            
            if($email_status == TRUE){
                
                
                $wpdb->update('wp_register_db', array( 'status' => 1, 'token' => $token ),array('email' => $mail));
            }
            
            $message = "";
 // }     
            

    }




}


add_action('wp_ajax_insert_register_now_applications', 'insert_register_now_applications');
add_action('wp_ajax_nopriv_insert_register_now_applications', 'insert_register_now_applications');

function insert_register_now_applications() {
    global $wpdb;

    $insert_now_first_name = trim($_POST['now_first_name']);

    $insert_now_last_name = trim($_POST['now_last_name']);

    $insert_now_tel = trim($_POST['now_tel']);
    $insert_now_email = trim($_POST['now_email']);

    $insert_now_token = $_POST['token'];

    //echo $first_name;

    $args = array(
        'first_name' => $insert_now_first_name,
        'last_name' => $insert_now_last_name,
        'telephone' => $insert_now_tel,
        'email' => $insert_now_email,
        'token' => $insert_now_token,
    );


    $result = $wpdb->insert('wp_register_email_db', $args);
}

add_action('wp_ajax_insert_register_social_applications', 'insert_register_social_applications');
add_action('wp_ajax_nopriv_insert_register_social_applications', 'insert_register_social_applications');

function insert_register_social_applications() {
    global $wpdb;

    $insert_candidate_name = trim($_POST['candidate_name']);
    $insert_candidate_tel = trim($_POST['candidate_tel']);
    $insert_candidate_email = trim($_POST['candidate_email']);
    $insert_candidate_desig = trim($_POST['candidate_desig']);
    $insert_candidate_org = trim($_POST['candidate_org']);
    $insert_candidate_token = $_POST['candidate_token'];

    //echo $first_name;

    $args = array(
        'name' => $insert_candidate_name,
        'telephone' => $insert_candidate_tel,
        'email' => $insert_candidate_email,
        'designation' => $insert_candidate_desig,
        'current_organization' => $insert_candidate_org,
        'token' => $insert_candidate_token
    );


    $result_social = $wpdb->insert('wp_social_media_registerations', $args);
}

/*
add_action('admin_menu','import_csv');

 function import_csv() {
        add_submenu_page('edit.php?post_type=jobs', 'Import Csv', 'Import csv', 'manage_options', 'importcsv', 'csv_import_callback');
    }
    
function csv_import_callback(){
    
    echo '<div class = "row">
            <div class = "col-md-3"><label>Upload CSV</label></div>
            <div class = "col-md-3">
            <input type="file" id="csv" name="csv" class="form-control" value="" placeholder="" />
            </div><div class = "col-md-3"><button class="import-csv" name="import">Import</button> </div>
            </div>
            ';
    
    if(isset($_POST['import'])){
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        
        if(!empty($_FILES['csv']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
            if(is_uploaded_file($_FILES['csv']['tmp_name'])){
                $csvFile = fopen($_FILES['csv']['tmp_name'], 'r');
                fgetcsv($csvFile);
                while(($line = fgetcsv($csvFile)) !== FALSE){
                    $sql = "SELECT id from wp_register_db where email = '$line[4]'";
                    $prevResult = $wpdb->get_results($sql);
                    if($prevResult->num_rows > 0){
                        $wpdb->update( 'wp_register_db', array('first_name' => $line[0],'mid_name' => $line[1], 'last_name' => $line[2], 'telephone' => $line[3] ),array('email'=>$line[4]));
                    }
                    else{
                        $wpdb->insert('wp_register_db', array('first_name' => $line[0], 'mid_name' => $line[1], 'last_name' => $line[2], 'telephone' => $line[3], 'email' => $line[4]));
                    }
                }
                fclose($csvFile);
            }
        }
        
    }
    
    
}    
 
 */