<?php get_header(); ?>
<div class="about-us">
    <div class="jobs-banner">
        <div class="overlay-jobs">
            <div class="section-title-jobs">
                <h2 class="section-caps-jobs"><center>ABOUT US</center></h2>
                <center><span class="underline-jobs"></span></center>
            </div>
        </div>
    </div>
    <div class=" about-content">
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Driven by an earnest desire to facilitate job opportunities to deserving individuals, and counsel and address the issues relating to careers and required skill sets, Selection Search was formed in March, 2006. Spearheaded by team of individuals who have had valuable experience in the field of education for more than a decade Selection search today caters to requirements for diverse industries.
                        It is one of the few search companies which is also facilitating educational institutions throughout the country in finding competent and capable leaders and educators.</p>
                </div>
                <div class="col-md-6">
                    <img src="<?php bloginfo('template_url') ?>/images/world-map.png" class="img-responsive">
                </div>
            </div>
            </div>
            </div>
            <div class="section our-aim">
              <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="about-aim">Our Aim</h2>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <h4>PARTNERSHIP</h4>
                        <p class="aim-desc">We act as an extension of our client’s office. We are knowledgeable partners providing decisive and dedicated support to our clients.</p>
                    </div>
                    <div class="col-md-3">
                        <h4>INTEGRITY</h4>
                        <p class="aim-desc">Our clients are demanding, their standards are high and so are ours. We honestly represent our clients to candidates and our candidates to clients. We do not promise what we cannot deliver.</p>
                    </div>
                    <div class="col-md-3">
                        <h4>CONFIDENTIALITY</h4>
                        <p class="aim-desc">We accord the highest respect to information that we have been entrusted with.</p>
                    </div>
                    <div class="col-md-3">
                        <h4>COMMITMENT</h4>
                        <p class="aim-desc">We remain committed to deliver high quality services to both candidates and clients by adequately understanding their needs.</p>
                    </div>
                </div>
               </div> 
            </div>
            <div class=" section our-method">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="about-method">Our Methodologies</h2>
                        <p class="method-desc">What sets us apart from other firms operating in a similar space is our complete involvement and commitment to the needs of the client. This is ensured by a 100% contact with all candidates either on telephone or by personal interview before they are forwarded to the clients. Selection Search today is catering to positions ranging from lower, middle to top management for its clients. This is done by a team of dedicated professionals at different levels.
                        </p>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h4>UNDERSTANDING CLIENT ORGANISATION</h4>
                        <p class="aim-desc">Understanding client organisation with regard to existing business activities, structure, culture, and future growth plans.</p>
                    </div>
                    <div class="col-md-4">
                        <h4>MAPPING POSITION</h4>
                        <p class="aim-desc">Mapping position – specific competencies with regard to the job viz : Title, principal accountabilities, key result areas, authority, reporting and supervisory relationships, working conditions/environment, job specifications, approximate compensation, location and other relevant details.</p>
                    </div>
                    <div class="col-md-4">
                        <h4>IDENTIFICATION OF CANDIDATES</h4>
                        <p class="aim-desc">Identification of candidates either from our existing database or through various searching strategies.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h4>COMPETENCY-BASED INTERVIEWING</h4>
                        <p class="aim-desc">Competency-based interviewing and relevant behavioural questionnaires to prepare a shortlist with recommendations to client.</p>
                    </div>
                    <div class="col-md-4">
                        <h4>HELPING NEGOTIATE THE OFFER</h4>
                        <p class="aim-desc">Helping negotiate the offer including compensation structures.</p>
                    </div>
                    <div class="col-md-4">
                        <h4>FOLLOW-UP WITH CLIENT & CANDIDATE</h4>
                        <p class="aim-desc">Follow-up with client and candidate subsequent to candidate joining the organisation to ensure satisfaction.</p>
                    </div>
                </div>
            </div>
           
           </div>
        </div>
    </div>
    <?php get_footer(); ?>