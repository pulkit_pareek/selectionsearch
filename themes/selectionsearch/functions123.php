<?php

include 'includes/wp_bootstrap_navwalker.php';

function selectionSearch_scripts() {


    wp_enqueue_style('wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600', false);

    wp_enqueue_style('fonts', get_template_directory_uri() . '/css/font-awesome.min.css');
	 wp_enqueue_style('SSOwl-carousal', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), '1.0.0', true);
 wp_enqueue_style('SSxOwl-carousal', get_template_directory_uri() . '/css/owl.theme.default.min.css', array(), '1.0.0', true);
    wp_enqueue_style('bootstrap-styles', get_template_directory_uri() . '/css/bootstrap.min.css', false, '1.0.0', 'all');

    //wp_enqueue_script('all-js', get_template_directory_uri() . '/js/all.min.js', array('jquery'), '1.0.0', true);

    wp_enqueue_style('style', get_stylesheet_uri());

    //wp_enqueue_script('jquery');

    wp_enqueue_script('selectionSearch_js', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', true);
wp_enqueue_script('SelectionSearch-carousal', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '1.0.0', true);

    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.3.2');
}

add_action('wp_enqueue_scripts', 'selectionSearch_scripts');


add_filter('wpcf7_validate_configuration', '__return_false');

add_theme_support('post-thumbnails');
add_theme_support('title-tag');

function primary_menu() {

    register_nav_menus(array(
        'primary' => __('Primary Menu', 'Top Navigation')
    ));
}

add_action('init', 'primary_menu');



add_action('init', 'create_slider');

function create_slider() {

    $slider_args = array(
        'labels' => array(
            'name' => __('Sliders'),
            'singular_name' => __('Sliders'),
            'add_new' => __('Add New Slider'),
            'add_new_item' => __('Add New Slider'),
            'edit_item' => __('Edit Slider'),
            'new_item' => __('Add New Slider'),
            'view_item' => __('View Slider'),
            'search_items' => __('Search Slider'),
            'not_found' => __('No slider found'),
            'not_found_in_trash' => __('No slider found in trash')
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        'menu_position' => 20,
        'supports' => array('title', 'editor', 'thumbnail')
    );

    register_post_type('slider', $slider_args);
}

add_action('init', 'featured_jobs');

function featured_jobs() {

    $job_args = array(
        'labels' => array(
            'name' => __('Jobs'),
            'singular_name' => __('Jobs'),
            'add_new' => __('Add New Job'),
            'add_new_item' => __('Add New Job'),
            'edit_item' => __('Edit Job'),
            'new_item' => __('Add New Job'),
            'view_item' => __('View Job'),
            'search_items' => __('Search Job'),
            'not_found' => __('No job found'),
            'not_found_in_trash' => __('No job found in trash')
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        'menu_position' => 20,
        'supports' => array('title', 'editor', 'thumbnail')
    );

    register_post_type('jobs', $job_args);
}

add_action('init', 'testimonials');

function testimonials() {

    $testimonial_args = array(
        'labels' => array(
            'name' => __('Testimonials'),
            'singular_name' => __('Testimonials'),
            'add_new' => __('Add New'),
            'add_new_item' => __('Add New Testimonial'),
            'edit_item' => __('Edit Testimonial'),
            'new_item' => __('Add New Testimonial'),
            'view_item' => __('View Testimonial'),
            'search_items' => __('Search Testimonial'),
            'not_found' => __('No testimonial found'),
            'not_found_in_trash' => __('No testimonial found in trash')
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        'menu_position' => 20,
        'supports' => array('title', 'editor', 'thumbnail')
    );

    register_post_type('testimonials', $testimonial_args);
}

if(function_exists('register_sidebar')){
	
	register_sidebar(array(
	'name' => 'Right Sidebar',
	'before_title' => '<h2>',
	'after_title' => '</h2>'
	
	));
} 

add_action('wp_ajax_insert_applications', 'insert_applications');
add_action('wp_ajax_nopriv_insert_applications', 'insert_applications');

function insert_applications() {
    global $wpdb;

    $first_name = trim($_POST['first_name']);
    $mid_name = trim($_POST['mid_name']);
    $last_name = trim($_POST['last_name']);
    $gender_male = trim($_POST['gender']);

    $post_applied = $_POST['post_applied'];
    $dob = trim($_POST['dob']);
    $father_name = trim($_POST['father_name']);
    $rec_org = trim($_POST['rec_org']);
    $rec_desig = trim($_POST['rec_desig']);
    $mother_name = trim($_POST['mother_name']);
    $mother_emp = trim($_POST['mother_emp']);
    $mother_desig = trim($_POST['mother_desig']);
    $mother_income = trim($_POST['mother_income']);
    $status = trim($_POST['status']);
    $spouse_name = trim($_POST['spouse_name']);
    $spouse_emp = trim($_POST['spouse_emp']);
    $spouse_desig = trim($_POST['spouse_desig']);
    $spouse_income = trim($_POST['spouse_income']);

    $name_children = $_POST['name_children'];
    $sex = $_POST['sex'];
    $age_children = $_POST['age_children'];
    $ed_children = $_POST['ed_children'];
    $school = $_POST['school'];

    $res_add = trim($_POST['res_add']);
    $res_add = htmlspecialchars($_POST['res_add']);
    $comm_add = trim($_POST['comm_add']);
    $comm_add = htmlspecialchars($_POST['comm_add']);
    $tel = trim($_POST['tel']);
    $email = trim($_POST['email']);
    $mother_tongue = trim($_POST['mother_tongue']);
    $other_lang = trim($_POST['other_lang']);

    $exam = $_POST['exam'];
    $institute = $_POST['institute'];
    $subjects = $_POST['subjects'];
    $from_date = $_POST['from_date'];
    $board = $_POST['board'];
    $to_date = $_POST['to_date'];
    $marks = $_POST['marks'];

    $achievements = trim($_POST['achievements']);
    $int_comm = trim($_POST['int_comm']);
    $int_art = trim($_POST['int_art']);
    $int_drama = trim($_POST['int_drama']);
    $int_debate = trim($_POST['int_debate']);
    $int_vocal = trim($_POST['int_vocal']);
    $int_read = trim($_POST['int_read']);
    $int_env = trim($_POST['int_env']);
    $int_dance = trim($_POST['int_dance']);
    $int_craft = trim($_POST['int_craft']);
    $int_instr = trim($_POST['int_instr']);
    $int_tour = trim($_POST['int_tour']);
    $int_defence = trim($_POST['int_defence']);
    $total_exp = trim($_POST['total_exp']);
    $sport = trim($_POST['sport']);
    $comp = trim($_POST['comp']);
    $exp_letter = trim($_POST['exp_letter']);
    $hobby = trim($_POST['hobby']);
    $sp_skill = trim($_POST['sp_skill']);

    $name_location = $_POST['name_location'];
    $profile = $_POST['profile'];
    $salary = $_POST['salary'];
    $from_duration = $_POST['from_duration'];
    $board_exp = $_POST['name_board'];
    $reason = $_POST['reason'];
    $upto = $_POST['upto'];
    $subject_class = $_POST['subject_class'];

    $class = $_POST['class'];
    $subject = $_POST['subject'];
    $workshop = trim($_POST['workshop']);
    $teacher_train = trim($_POST['teacher_train']);
    $digital = trim($_POST['digital']);
    $tech = trim($_POST['tech']);
    $salary_exp = trim($_POST['salary_exp']);
    $join = trim($_POST['join']);

    $name_ref = $_POST['name_ref'];
    $occ_ref = $_POST['occ_ref'];
    $add_ref = $_POST['add_ref'];

    $args = array(
        'first_name' => $first_name,
        'mid_name' => $mid_name,
        'last_name' => $last_name,
        'gender' => $gender_male,
        'post_applied' => $post_applied,
        'dob' => $dob,
        'father_name' => $father_name,
        'rec_org' => $rec_org,
        'rec_desig' => $rec_desig,
        'mother_name' => $mother_name,
        'mother_emp' => $mother_emp,
        'mother_desig' => $mother_desig,
        'mother_income' => $mother_income,
        'marital_status' => $status,
        'spouse_name' => $spouse_name,
        'spouse_emp' => $spouse_emp,
        'spouse_desig' => $spouse_desig,
        'spouse_income' => $spouse_income,
        'residence_add' => $res_add,
        'comm_add' => $comm_add,
        'telephone' => $tel,
        'email' => $email,
        'mother_tongue' => $mother_tongue,
        'other_lang' => $other_lang,
        'achievements' => $achievements,
        'interest_comm' => $int_comm,
        'interest_art' => $int_art,
        'interest_drama' => $int_drama,
        'interest_debate' => $int_debate,
        'interest_music' => $int_vocal,
        'interest_read' => $int_read,
        'interest_env' => $int_env,
        'interest_dance' => $int_dance,
        'interest_craft' => $int_craft,
        'interest_instrument' => $int_instr,
        'interest_tours' => $int_tour,
        'interest_defence' => $int_defence,
        'sports' => $sport,
        'comp_knowledge' => $comp,
        'exp_writing' => $exp_letter,
        'hobby' => $hobby,
        'special_skills' => $sp_skill,
        'total_exp' => $total_exp,
        'workshops' => $workshop,
        'teacher_training' => $teacher_train,
        'digital_content' => $digital,
        'tech_support' => $tech,
        'salary_expect' => $salary_exp,
        'joining' => $join
    );


   

    $result = $wpdb->insert('wp_applications', $args);

    //$last_id = $wpdb->insert_id();

   $last_id = $wpdb->insert_id;

    $children_name = implode(",",$name_children);
    $children_sex = implode(",",$sex);
    $children_age = implode(",",$age_children);
    $children_ed = implode(",",$ed_children);
    $children_school = implode(",",$school);

   $children = array(
        'user_id' => $last_id,
        'name' => $children_name,
        'sex' => $children_sex,
        'age' => $children_age,
        'education' => $children_ed,
        'school_college' => $children_school
    );

    $result_children = $wpdb->insert('wp_children', $children);

    $exam_name = implode(",",$exam);
    $exam_institute = implode(",",$institute);
    $exam_subjects = implode(",",$subjects);
    $exam_from = implode(",",$from_date);
    $exam_to = implode(",",$to_date);
    $exam_board = implode(",",$board);
    $exam_marks = implode(",",$marks);

    $qualification = array(
        'user_id' => $last_id,
        'exam' => $exam_name,
        'from_date' => $exam_from,
        'to_date' => $exam_to,
        'institute' => $exam_institute,
        'board' => $exam_board,
        'marks' => $exam_marks,
        'subjects' => $exam_subjects
    );

    $result_qualification = $wpdb->insert('wp_qualifications', $qualification);

    $exp_name = implode(",",$name_location);
    $exp_profile = implode(",",$profile);
    $exp_salary = implode(",",$salary);
    $exp_from = implode(",",$from_duration);
    $exp_upto = implode(",",$upto);
    $exp_board = implode(",",$board_exp);
    $exp_reason = implode(",",$reason);
    $exp_class = implode(",",$subject_class);

    print_r($experience = array(
        'user_id' => $last_id,
        'name_location' => $exp_name,
        'from_duration' => $exp_from,
        'upto_duration' => $exp_upto,
        'profile' => $exp_profile,
        'board' => $exp_board,
        'subject_class' => $exp_class,
        'salary' => $exp_salary,
        'reason' => $exp_reason
    ));

    $result_experience = $wpdb->insert('wp_experience', $experience);

    $name_subject = implode(",",$subject);
    $name_class = implode(",",$class);

    $sub = array(
        'user_id' => $last_id,
        'class' => $name_class,
        'subject' => $name_subject
    );

    $result_sub = $wpdb->insert('wp_subjects', $sub);

    $ref_name = implode(",",$name_ref);
    $ref_occ = implode(",",$occ_ref);
    $ref_add = implode(",",$add_ref);

    $ref = array(
        'user_id' => $last_id,
        'name' => $ref_name,
        'occupation' => $ref_occ,
        'address' => $ref_add
    );

    $result_ref = $wpdb->insert('wp_reference', $ref);
}
