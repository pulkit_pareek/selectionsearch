<!DOCTYPE html>
<html>
<head>
    
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header>
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="top-links1">
                        <li><i class="fa fa-phone" aria-hidden="true"></i> : +91 7291987087</li>
                       <li><a href="mailto:anuradha@selectionsearch.in"><i class="fa fa-envelope-o" aria-hidden="true"></i> : anuradha@selectionsearch.in</a></li>
                        
                        </ul>
                   <!-- <ul class="top-links2">
                        <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>:INFO@JOBPORTAL.COM</a></li>
                    </ul>-->
                </div>
                <div class="col-sm-6">
                    <ul class="social-links">
						                        <li><a href="#" class="pinterest"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                        <li><a href="#" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                       
                        

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="img-responsive">
                
            </div>
            
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a  href="<?php echo SITE_URL(); ?>">
                    <img src="<?php bloginfo('template_url')?>/images/logo.png" class="logo"></a>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    
                      <?php   wp_nav_menu(array(
                                'menu' => 'primary',
                                'theme_location' => 'primary',
                                'depth' => 3,
                                'container' => false,
                                'menu_class' => 'nav navbar-nav',
                                'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                'walker' => new wp_bootstrap_navwalker())
                            );
                            ?>
                       <!-- <li class="active"><a href="#">HOME <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">JOBS</a></li>
                        <li><a href="#">ABOUT</a></li>
                        <!--<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DROPDOWN <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <li><a href="#">OUR PARTNERS</a></li>
                        <li><a href="#">TESTIMONIALS</a></li>
                        <li><a href="#">CONTACT</a></li>-->
                        <!--ul class="nav navbar-nav">
                        <li><button class="btn-default">Login</button></li>
                        <li><button class="btn-default">Submit Jobs</button></li>
                    </ul-->

                    

                </div>
                <!-- /.navbar-collapse -->
        
            <!-- /.container-fluid -->
        </nav>

    </div>
    
</header>
