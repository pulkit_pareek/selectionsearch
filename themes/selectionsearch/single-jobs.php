<?php get_header();?>

 <div class="single-jobs-banner">
    
               <div class="overlay-jobs">
            <div class="container">
                
                <center><span class="underline-jobs"></span></center>
                <div class="row">
                    <div class="col-md-6">
                      <?php  if (have_posts()) :
                    
            ?>
             <?php while (have_posts()) : the_post(); 
				$responsibilities = get_field('responsibilities');
				$requirements = get_field('requirements');
				$edu_qualifications = get_field('educational_qualifications');
				$salary = get_field('min_max_salary');
                                $job = get_field('job_location');
?>
           <h2 class="section-caps-jobs"><?php the_title(); ?></h2>
           <?php if($job != ""){?>
            <h4> JOB LOCATION: &nbsp;<?php the_field('job_location'); ?></h4>
            <?php } ?>
            
                </div>
                    <!--div class="col-md-6 apply-top-btn">
                        <a href="<?php echo SITE_URL(); ?>/apply?id=<?php the_field('job_id'); ?>" class="apply-job">Apply to this job</a> 
                    </div-->
               </div>
                <?php endwhile; ?> 
                       <?php endif;
                       wp_reset_query();?> 
            </div>
 </div>
</div>
<div class="sections single-jobs-desc">
     <div class="container">
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12">
          <div class="img-responsive">
                                    <?php
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail();
                                    }
                                    ?>
                                   </div>
        <div class="job-desc">
            <h4>JOB DESCRIPTION</h4> <p><?php  the_content(); ?></p>
			<?php 	if($responsibilities != ""){?>
            <h4>RESPONSIBILITIES</h4> <p><?php  the_field('responsibilities'); ?></p>
			<?php } ?>
			<?php 	if($requirements != ""){?>
            <h4>REQUIREMENTS</h4> <p><?php  the_field('requirements'); ?></p>
		    <?php } ?>
			<?php 	if($edu_qualifications != ""){?>
            <h4>EDUCATIONAL QUALIFICATIONS</h4> <p><?php  the_field('educational_qualifications'); ?></p>
			<?php } ?>
			<?php 	if($salary != ""){?>
            <h4>MIN AND MAX SALARY:</h4> <p><?php  the_field('min_max_salary'); ?></p>
			<?php } ?>
        </div>
            <div class="row">
        <div class="col-md-12">
            <!--div class="bottom-job">
                <a href="<?php echo SITE_URL(); ?>/apply?id=<?php the_field('job_id'); ?>" class="apply-job-bottom">Apply to this job</a> </div--> </div>
    </div>
    </div>
         <div class="col-md-3 col-sm-3 col-xs-12"> 
            <div class="jobs-sidebar">
                <h3>Apply Now</h3><br>
                <div class="jobs-form">
                    
                    <form action="" method="post" id="sidebarForm">
                        <input type="text" class="form-control" id="candidate_name" name="candidate_name" placeholder="Name" required ><br>
                        <input type="email" class="form-control" id="candidate_email" name="candidate_email" placeholder="Email" required ><br>
                        <input type="text" class="form-control" id="candidate_tel" name="candidate_tel" placeholder="Telephone" required ><br>
                        <input type="text" class="form-control" id="candidate_desig" name="candidate_desig" placeholder="Designation"><br>
                        <input type="text" class="form-control" id="candidate_org" name="candidate_org" placeholder="Current Organization"><br>
                        <input type="submit" class="apply-btn" id="submit-details" name="submit-details" value="SUBMIT"><br>
                        <input type="hidden" name="candidate_token" id="random_token"><br>
                        <input type="hidden" value="<?php echo SITE_URL(); ?>/wp-admin/admin-ajax.php" id="url" />
                    </form>
                </div>
                <h3>Other Jobs</h3>
                 <?php
                query_posts(array('post_type' => 'jobs', 'posts_per_page' => 4));

                if (have_posts()) :
                    ?>
                    <?php while (have_posts()) : the_post(); ?>
				<div class="links_more">
                 <h4 class="section-caps-jobs"><?php the_title(); ?></h4>
                 <a href="<?php the_permalink(); ?>">Read more</a>
				</div>
                 <?php endwhile; ?> 
                <?php
                endif;
                wp_reset_query();
                ?> 
            </div>
        </div>
        </div>
       
    </div>
    </div>
<script>
    function random_token() {
  var min = 1;
  var max = 9999999999;
  var num = Math.floor(Math.random() * (max - min + 1)) + min;
  var timeNow = new Date().getTime();
  document.getElementById('random_token').value = num + '_' + timeNow;
}
window.onload = random_token;

$('#sidebarForm').submit(function (e) {
    e.preventDefault();
    //alert("test");
    	var file = event.target.files;
       //var formData = new FormData(this);
       var data = new FormData(this);
         //console.log(data);
        data.append("action", "insert_register_social_applications");
        $.each(file, function(key, value)
    	{
      		data.append("insert_register_social_applications", value);
    	});
   //var file = formData.append('resume', document.getElementById("resume").files[0]);
    var name = $('#candidate_name').val();
    var tel = $('#candidate_tel').val();
    var email = $('#candidate_email').val();
    var desig = $('#candidate_desig').val();
    var curr_org = $('#candidate_org').val();
    var token = $('#random_token').val();
    
    var url = $('#url').val();
    //var datastr = 'action=insert_applications&' + $(this).serialize();
    $(".loader").show();
    //console.log(datastr);
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        //dataType: 'json',
        processData: false,
        contentType: false,
        
        success: function (result) { 
           alert(result);
           if(result=='0') {
               alert("Your form is submitted! Thank you for applying.");
               document.getElementById("sidebarForm").reset();
               window.location ="http://localhost/wordpress/register-social?key="+token;;
                $(".loader").hide();location.href
           //$('#applyForm').reset();
       }
           else{alert("Please verify your form");
            $(".loader").hide();}

        }
    });


});


    </script>


<?php get_footer();?>