
<?php 
/* Template Name: home */


get_header(); ?>

<!--Banner section-->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php $slider = get_posts(array('post_type' => 'slider', 'posts_per_page' => -1)); ?>
        <?php $count = 0; ?>
        <?php foreach ($slider as $slide): ?>
            <div class="item <?php echo ($count == 0) ? 'active' : ''; ?>">
                <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($slide->ID)) ?>" class="img-responsive" style="height:100%;width:100%; "/>
            </div>
            <?php $count++; ?>
        <?php endforeach; ?>
    </div>

    <!--div class="carousel-caption">
        <div class="overlay">

            <div class="carousel-title">
                
                <form>
                    <div class="row form" > 
                        <div class="col-sm-4">
                            <div class="dropdown1">
                                <input type="text" class="form-control input_cat"  list="category" placeholder="Select Category"/>
                                
                                <datalist id="category">
                                    <option >Category 1</option>
                                    <option >Category 2</option>
                                    <option >Category 3</option>
                                    <option >Category 4</option>
                                </datalist>
                                <!--select class="form-control"><option></option>
                                    <option></option>
                                    <option></option>
                                    <option></option>
                                </select>

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="dropdown2">
                                <input type="text" class="form-control input_cat" id="location" list="Location" placeholder="Select Location"/>
                                
                                <datalist id="Location">
                                    <option >Location 1</option>
                                    <option >Location 2</option>
                                    <option >Location 3</option>
                                    <option >Location 4</option>
                                </datalist>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="search-box2">
                                <input type="text" class="search" placeholder="Search" ><button type="submit" class="search-button"><i class="fa fa-search fa-lg" aria-hidden="true"></i>
                                </button>  
                            </div>
                        </div>
                    </div>
                </form> 
            </div>
        </div>

    </div-->
</div>

</div>

</div>
<!--End of Banner-->
<div class="section section-featured">
    <div class="container">
        <div class="main-body">

            <div class="section-title">
                <h2 class="section-caps"><center>FEATURED JOBS</center></h2>
                <center><span class="underline"></span></center>
            </div>
             <div class="row grids">
                <?php
                query_posts(array('post_type' => 'jobs', 'posts_per_page' => 4));
             
                if (have_posts()) :
                    ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-sm-3">

                            <div class="grid_jobs">
                                <div class="img-responsive img_dash">
                                    <?php
                                     $job = get_field('job_location');
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail();
                                    } else { ?>
                          <img src="http://selectionsearch.in/wp-content/uploads/2017/12/default_img.png" > 
                                   <?php }  ?>
                                    <!--<img src="" alt="image" class="img-responsive">--></div>
                                <div class="featured-box">
                                    <div class="featured-title">
                                        <span class="featured-box-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span></div>
                                    <div class="bookmark"><?php if($job != "") { ?><span><i class="fa fa-map-marker" aria-hidden="true"></i></span><span class="job-location"><?php the_field('job_location'); ?></span> <?php } ?>
                                    </div>
                                    <div class="featured-box-desc">
                                        <p><?php the_field('short_description'); ?></p>
                                    </div>
                                    <div class="featured-box-btn">
                                        <div class="list-inline listing_jobs"><div class="jobs_btn"><a href="<?php the_permalink(); ?>">VIEW MORE</a></div><div class="jobs_btn"><a href="<?php echo SITE_URL(); ?>/apply?id=<?php the_field('job_id'); ?>">APPLY NOW</a></div></div>
                                    </div>
                                   
                                </div>
                            </div>

                        </div>
                    <?php endwhile; ?> 
                    
                <?php
                endif;
                wp_reset_query();
                ?>
 
                </div>
          


        </div>

    </div>

</div>
<div class="section welcome-section">
    <div class="container">
        <div class="section-title2">
            <h2 class="section-caps2"><center>WELCOME TO SELECTION SEARCH</center></h2>
            <center><span class="underline2"></span></center>
        </div>
        <div class="welcome-descr">

            <p>Driven by an earnest desire to facilitate job opportunities to deserving individuals, and counsel and address the issue relating to careers and required skill sets, Selection Search was formed in March, 2006.
                Spearheaded by a team of individuals who have had valuable experience in the field of education for more than a decade Selection Search today caters to requirements fort diverse industries.</p>
            <p>It is one of the few search companies which is also facilitating educational institutions throughout the country in finding competent and capable leaders and educators.</p>
        </div>
    </div>
</div>
<div>
    <div class="testimonial-section">
        <div class="section overlay2">
            <div class="section-title3">
                <h2 class="section-caps3"><center>TESTIMONIALS</center></h2>
                <center><span class="underline3"></span></center>
            </div>
            <div class="testimonial-descr">
                <?php
                query_posts(array('post_type' => 'testimonials', 'posts_per_page' => 1));

                if (have_posts()) :
                    ?>
                    <div class="container">
                        <div class="content_t">
    <?php while (have_posts()) : the_post(); ?>
                                <span class="quote-left"><i class="fa fa-quote-left fa-lg" aria-hidden="true"></i></span>    
                                <p><?php the_content(); ?></p>
                                <!--<p>Her
            meticulous selection brought forward an impressive short list from the best
            known schools in the country. Her careful reference checking made the selection
            thorough.</p>--> <span class="quote-right"><i class="fa fa-quote-right fa-lg" aria-hidden="true"></i></span>  
                            </div>
                        </div>
                        <div class="testi-title"><h3><?php the_title(); ?></h3><p><?php the_field('designation'); ?></p></div>
                <?php endwhile; ?>         
                </div>
            <?php
            endif;
            wp_reset_query();
            ?>
        </div> 


    </div>  
</div>
<div class="section partners">

    <div class="container">
        <div class="section-title4">
            <h2 class="section-caps4"><center>Our Educational Partners</center></h2>
            <center><span class="underline4"></span></center>
        </div> 
        <div class="educational-partners">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/8.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/12.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/10.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/11.png" alt="..." ></div></div>

                    </div>

                    <div class="item">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/7.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/13.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/17.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/20.png" alt="..." ></div></div>

                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/21.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/14.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/18.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/9.png" alt="..." ></div></div>


                    </div>
                     <!--div class="item">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/14.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/15.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/16.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/17.png" alt="..." ></div></div>


                    </div>
                     <div class="item">
                                               <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/sbs.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/19.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/20.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/21.png" alt="..." ></div></div>


                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/22.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/23.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/kc.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/heritage.png" alt="..." ></div></div>


                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/kangaroo.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/kanpur.png" alt="..." ></div></div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="partner-logo">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/partners/education/ps.png" alt="..." ></div></div>



                    </div-->


                    <!-- Controls -->
                    <div class="arrow-left">
                        <a class="left" href="#carousel-example-generic" data-slide="prev">
                            <span><i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </div>
                    <div class="arrow-right">
                        <a class="right" href="#carousel-example-generic" data-slide="next">
                            <span><i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </div>  


                </div>

            </div>
        </div>
    </div>
</div>


<div class="section">   
    <div class="container">
        <div class="section-title4">
            <h2 class="section-caps4"><center>Our Corporate Partners</center></h2>
            <center><span class="underline4"></span></center>
        </div> 
        <div class="corporate-partners">
            <div class="owl-carousel owl-theme">
                        <div class="item"><img  src="<?php bloginfo('template_url') ?>/images/partners/corporate/a.png" alt="..."></div>
                        <div class="item"><img  src="<?php bloginfo('template_url') ?>/images/partners/corporate/2.png" alt="..."></div>

                        <div class="item"><img  src="<?php bloginfo('template_url') ?>/images/partners/corporate/m.png" alt="..."></div>
                        <div class="item"><img  src="<?php bloginfo('template_url') ?>/images/partners/corporate/4.png" alt="..."></div>
                        <div class="item"><img  src="<?php bloginfo('template_url') ?>/images/partners/corporate/5.png" alt="..."></div>

                    </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>	