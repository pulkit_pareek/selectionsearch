<?php get_header(); ?>
<div class=" our-partners">
    <div class="jobs-banner">
        <div class="overlay-jobs">
            <div class="section-title-jobs">
                <h2 class="section-caps-jobs"><center>OUR PARTNERS</center></h2>
                <center><span class="underline-jobs"></span></center>
            </div>
        </div>
    </div>
    <div class="section">
    <div class="container">
        <div class="corp-partners">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/1.png" alt="bharti" class="img-responsive" style="margin:auto;padding:10px;">    
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/2.png" alt="pioneer" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/3.png" alt="ogaan" class="img-responsive" style="margin:auto;padding:10px;"> 
                </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/4.png" alt="media" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
            </div>
            <div class="row">
               <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/21.png" alt="media" class="img-responsive" style="margin:auto;padding:10px;">
                </div><div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/22.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/6.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/7.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
            </div>
            <div class="row">
                
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/10.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/11.png" alt="media" class="img-responsive" style="margin:auto;padding:10px;">
                </div><div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/12.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/13.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
            </div>
            <div class="row">
                
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/14.png" alt="media" class="img-responsive" style="margin:auto;padding:10px;">
                </div><div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/15.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/16.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/17.png" alt="media" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/18.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/20.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/kc.png" alt="kc_high" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
<div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/gyanshree.png" alt="kc_high" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
            </div>
	<div class="row">
<div class="col-md-3 col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('template_url') ?>/images/partners/10.png" alt="sbs_noida" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
	</div>
            <!--div class="row">
               
                <div class="col-md-4">
                    <img src="<?php bloginfo('template_url') ?>/images/partner_image/23.png" alt="macmillan" class="img-responsive" style="margin:auto;padding:10px;">
                </div>
            </div-->
        </div>
    </div>
  </div>
</div>
</div>
<?php get_footer(); ?>