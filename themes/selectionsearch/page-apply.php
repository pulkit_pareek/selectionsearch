<?php get_header(); 
$id =$_GET['id'];?>
<div class="jobs-banner">
               <div class="overlay-jobs">
            <div class="section-title-jobs">
                <h2 class="section-caps-jobs"><center>APPLY NOW</center></h2>
                <center><span class="underline-jobs"></span></center>
            </div>
               </div>
            </div>
    <div class="section apply-now">
    <div class="container">
        <form action="" method="post" id="applyForm" enctype="multipart/form-data">
            
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Post applied for:</label>
                    </div>
                    <div class="col-md-3">
                        <input id="post_applied" name="post_applied" class="form-control" value="<?php echo $id; ?>" readonly/>
                            
                    </div>
                    <div class="col-md-3">
                        <a href="<?php echo SITE_URL(); ?>/jobs" class="change-job">Change</a>
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Name:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="first_name" name="first_name" class="form-control" value="" placeholder="First Name" required />
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="mid_name" name="mid_name" class="form-control" value="" placeholder="Middle Name" />
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="last_name" name="last_name" class="form-control" value="" placeholder="Last Name" required />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Gender:</label>
                    </div>
                    <div class="col-md-3">
                        <label for="radio_male"><input name="gender" id="radio_male" value="male" type="radio" required/> Male </label>&nbsp;
                        <label for="radio_female"><input name="gender" id="radio_female" value="female" type="radio" required/> Female </label>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Date of Birth:</label>
                    </div>
                    <div class="col-md-3">
                        <input id="dob" name="dob" class="form-control" type="date" required />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Father's Name:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="father_name" name="father_name" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Recent Employment:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="rec_org" name="rec_org" class="form-control" value="" placeholder="Organization" required />
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="rec_desig" name="rec_desig" class="form-control" value="" placeholder="Designation" required />
                    </div>
                </div>

                <div class="row apply">
                    <div class="col-md-3">
                        <label>Mother's Name:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="mother_name" name="mother_name" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Employment:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="mother_emp" name="mother_emp" class="form-control" value="" placeholder="Organization" />
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="mother_desig" name="mother_desig" class="form-control" value="" placeholder="Designation" />
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="mother_income" name="mother_income" class="form-control" value="" placeholder="Income(P.A)" />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Marital Status:</label>
                    </div>
                    <div class="col-md-4">
                        <label for="status1"><input id="status1" name="status" value="single" type="radio" required/> Single </label>&nbsp;
                        <label for="status2"><input id="status2" name="status" value="married" type="radio" required/> Married </label>&nbsp;
                        <label for="status3"><input id="status3" name="status" value="separated" type="radio" required/> Separated </label>&nbsp;
                        <label for="status4"><input id="status4" name="status" value="widowed" type="radio" required/> Widowed</label>
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Spouse's Name:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="spouse_name" name="spouse_name" class="form-control" value="" placeholder="" />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Employment:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="spouse_emp" name="spouse_emp" class="form-control" value="" placeholder="Organization" required/>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="spouse_desig" name="spouse_desig" class="form-control" value="" placeholder="Designation" required/>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="spouse_income" name="spouse_income" class="form-control" value="" placeholder="Income(P.A)" required/>
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>No. of Children:</label>
                    </div>
                    <div class="col-md-9" id="cust_brand">
                        <div class="row" id="add-container">
                            <div class="col-md-3">
                                <input type="text" id="name_children" name="name_children[]" class="form-control" value="" placeholder="Name" />
                            </div>
                            <div class="col-md-2">
                                <input type="text" id="sex" name="sex[]" class="form-control" value="" placeholder="Sex" required/>
                            </div>
                            <div class="col-md-2">
                                <input type="text" id="age_children" name="age_children[]" class="form-control" value="" placeholder="Age" />
                            </div>
                            <div class="col-md-2">
                                <input type="text" id="ed_children" name="ed_children[]" class="form-control" value="" placeholder="Education" />
                            </div>
                            <div class="col-md-2">
                                <input type="text" id="school" name="school[]" class="form-control" value="" placeholder="School/College" />
                            </div>
                            <div class="col-md-1"> <a href='javascript:void(0);' id='plus_cust_brnd'><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a></div>
                        </div>
                    </div>

                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Permanent Address:</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="res_add" name="res_add" class="form-control" rows="" cols="" placeholder="" required ></textarea>
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Address for Communication:</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="comm_add" name="comm_add" class="form-control" rows="" cols="" placeholder="" required ></textarea>
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Telephone No.:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="tel" name="tel" class="form-control" value="" placeholder="" required />
                    </div>
                    <div class="col-md-3">
                        <label>Email ID:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="email" id="email" name="email" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Mother Tongue:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="mother_tongue" name="mother_tongue" class="form-control" value="" placeholder="" required />
                    </div>
                    <div class="col-md-3">
                        <label>Other Language(s) Known:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="other_lang" name="other_lang" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                <div class="row apply">

                    <div class="col-md-3">
                        <label>Qualifications:</label>
                        <br /><br />    
                    </div>
                    <div class="col-md-9" id="qualify">
                         <div class="row" id="add-qualify">
                            <div class="col-md-4">
                                <label>Examination:</label>
                                <br />
                                <select id="exam" name="exam[]" class="form-control" required >
                                    <option  value="Secondary">Secondary</option>
                                    <option id="sen_sec" name="sen_sec" class="form-control" value="Senior Secondary">Senior Secondary</option>
                                    <option id="grad" name="grad" class="form-control" value="Graduation">Graduation</option>
                                    <option id="post_grad" name="post_grad" class="form-control" value="Post Graduation">Post Graduation</option>
                                    <option id="diploma" name="diploma" class="form-control" value="Diploma">Diploma</option>
                                    <option id="b_ed" name="b_ed" class="form-control" value="B.Ed">B.Ed.</option>
                                    <option id="m_ed" name="m_ed" class="form-control" value="M.Ed">M.Ed.</option>
                                    <option id="other" name="other" class="form-control" value="Others">Others</option>
                                </select>
                                <label>%</label>
                                <input type="text" id="marks" name="marks[]" class="form-control" value="" placeholder="" required />
                                <br />
                                <label>Board:</label>
                                <input type="text" id="board" name="board[]" class="form-control" value="" placeholder="" required /> 
                            </div>

                            <div class="col-md-4">
                                <label>From:</label>
                                <br />

                                <input type="date" id="from_date" name="from_date[]" class="form-control" class="form-control" value="" placeholder="" required />
                                <label>Institute:</label>
                                <input type="text" id="institute" name="institute[]" class="form-control" value="" placeholder="" required />
                                <br />
                            </div>

                            <div class="col-md-4">
                                <label>To:</label>
                                <br />
                                <input type="date" id="to_date" name="to_date[]" class="form-control" class="form-control" value="" placeholder="" required /> 
                                <label>Subjects:</label>
                                <input type="text" id="subjects" name="subjects[]" class="form-control" value="" placeholder="" required /><br />
                                 <a href='javascript:void(0);' id='plus_qualify'><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a>
                            </div>
                            

                        </div>                                    
                    </div>                                 
                </div>

                <!--div class="row apply">
                    <div class="col-md-3">
                        <label>Achievements in Games / Sports / Athletics / NCC/ Scouting /Girl Guide etc. Give Details.</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="achievements" name="achievements" class="form-control" row applys="" cols="" placeholder=""  ></textarea>
                    </div>
                </div-->
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Extra Curricular Activity :</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" id="achievements" name="achievements" class="form-control" value="" placeholder=" Music, Dance, Drama etc."  />
                        </div>
                    <!--input type="text" id="int_comm" name="int_comm" class="form-control" value="" placeholder="Community Work"  />
                        <input type="text" id="int_art" name="int_art" class="form-control" value="" placeholder="Art (Specify)"  />
                        <!--input type="text" id="int_drama" name="int_drama" class="form-control" value="" placeholder="Dramatics" required /-->
                        <!--input type="text" id="int_debate" name="int_debate" class="form-control" value="" placeholder="Debate/Elocution/Recitation"  />
                        <!--input type="text" id="int_vocal" name="int_vocal" class="form-control" value="" placeholder="Vocal Music" required /-->
                        <!--input type="text" id="int_read" name="int_read" class="form-control" value="" placeholder="Reading"  />
                        <input type="text" id="int_env" name="int_env" class="form-control" value="" placeholder="Environmental Activities"  />
                    
                    <div class="col-md-4">
                        
                        
                        <input type="text" id="int_craft" name="int_craft" class="form-control" value="" placeholder="Crafts (Specify)"  />
                        <!--input type="text" id="int_instr" name="int_instr" class="form-control" value="" placeholder="Instrumental Music (Specify)" required /-->
                        <!--input type="text" id="int_tour" name="int_tour" class="form-control" value="" placeholder="Participation in Tours, Camps etc (Domestic /Outdoor)"  />
                        <input type="text" id="int_defence" name="int_defence" class="form-control" value="" placeholder="Self Defense/Martial Arts"  />
                    </div-->
                </div>
                <!--`div class="row apply">
                    <div class="col-md-3">
                        <label>Sports you like to play:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="sport" name="sport" class="form-control" value="" placeholder=""  />
                    </div>
                </div-->
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Computer Knowledge:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="comp" name="comp" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Any experience of writing for in house publication of school:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" id="exp_letter" name="exp_letter" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Hobbies:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" id="hobby" name="hobby" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Any special skills (relevant to the post applied for) that you possess:</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="sp_skill" name="sp_skill" class="form-control" row applys="" cols="" placeholder="" required ></textarea>
                    </div>
                </div>
                <div class="row apply">

                    <div class="col-md-3">
                        <label>Work Experience:</label>

                    </div>
                    <div class="col-md-9" id="exp">
                        <div class="row" id="add-exp">
                            <div class="col-md-4">
                                <label>Name & Location of the Institution:</label>
                                <br />
                                <textarea id="name_location" name="name_location[]" class="form-control" row applys="" cols="" placeholder="" required ></textarea>
                                <label>Job Profile:</label>
                                <input type="text" id="profile" name="profile[]" class="form-control" value="" placeholder="" required />
                                <br />
                                <label>Salary Drawn:</label>
                                <input type="text" id="salary" name="salary[]" class="form-control" value="" placeholder="" required />
                            </div>

                            <div class="col-md-4">
                                <label>From:</label>
                                <br />

                                <input type="date" id="from_duration" name="from_duration[]" class="form-control" class="form-control" value="" placeholder="" required />
                                <br />
                                <label>Name of Board:</label>
                                <input type="text" id="board" name="name_board[]" class="form-control" value="" placeholder="" required />
                                <label>Reason for Change:</label>
                                <input type="text" id="reason" name="reason[]" class="form-control" value="" placeholder="" required />
                            </div>
                            <div class="col-md-4">
                                <label>Upto:</label>
                                <br />
                                <input type="date" id="upto" name="upto[]" class="form-control" class="form-control" value="" placeholder="" required />
                                <br />
                                <label>Subjects & Classes taught:</label>
                                <input type="text" id="subject_class" name="subject_class[]" class="form-control" value="" placeholder="" required />
                                <br />
                                <a href='javascript:void(0);' id='plus_exp'><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row apply">
                    <div class="col-md-3">
                        <label>Total Experience(in yrs.):</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="total_exp" name="total_exp" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Which subject(s), besides the area of your specialization, would you like to teach? Please mention them in order of preference:</label>
                    </div>
                    <div class="col-md-9" id="subject">
                        <div class="row" id="add-subject">
                            <div class="col-md-3">
                                <input type="text" id="classs" name="class[]" class="form-control" value="" placeholder="Class" required />
                            </div>
                            <div class="col-md-3">
                                <input type="text" id="subject" name="subject[]" class="form-control" value="" placeholder="Subject" required />
                            </div>
                            <div class="col-md-3">
                                <a href='javascript:void(0);' id='plus_subject'><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Please provide following details about workshops attended by you in the tenure of your service. Name/Topic of the workshop, Duration, Name & Qualification of the speaker, Venue of the Workshop, Key Learning, Certification if any.:</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="workshop" name="workshop" class="form-control" rows="" cols="" placeholder="" required ></textarea>
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Specify if you have conducted teacher training workshops at School in or after school hours:</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="teacher_train" name="teacher_train" class="form-control" rows="" cols="" placeholder="" required ></textarea>
                    </div>
                </div>
                <!--div class="row apply">
                    <div class="col-md-3">
                        <label>Your views on usage of digital content in classroom:</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="digital" name="digital" class="form-control" row applys="" cols="" placeholder=""  ></textarea>
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Have you used any technological support in your lessons:</label>
                    </div>
                    <div class="col-md-6">
                        <textarea id="tech" name="tech" class="form-control" row applys="" cols="" placeholder=""  ></textarea>
                    </div>
                </div>
                <div class="row apply">
                    <div class="col-md-3">
                        <label>What is your salary expectations?</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="salary_exp" name="salary_exp" class="form-control" value="" placeholder=""  />
                    </div>
                </div-->
                <!--div class="row apply">
                    <div class="col-md-3">
                        <label>If selected, how soon can you join?</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="join" name="join" class="form-control" value="" placeholder=""  />
                    </div>
                </div-->
                <!--div class="row apply">
                    <div class="col-md-12">
                        <label>Give below the names of two references, not related to you, whom we may contact. Both should be holding responsible positions. At least one of whom should be in teaching occupation.</label>
                    </div>

                </div-->

                <!--div class="row apply">
                    <div class="col-md-9" id="referee">
                        <div class="row" id="add-referee">
                            <div class="col-md-3">
                                <input type="text" id="name_ref" name="name_ref[]" class="form-control" value="" placeholder="Name of Referee"  />
                            </div>

                            <div class="col-md-3">
                                <input type="text" id="occ_ref" name="occ_ref[]" class="form-control" value="" placeholder="Occupation"  />
                            </div>
                            <div class="col-md-3">
                                <input type="text" id="add_ref" name="add_ref[]" class="form-control" value="" placeholder="Address & Phone no."  />
                            </div>
                            <div class="col-md-3">
                                <a href='javascript:void(0);' id='plus_referee'><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a>
                            </div>
                        </div> </div>
                </div-->
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Upload Resume :</label>
                    </div>
                    <div class="col-md-3">
                        <input type="file" id="resume" name="resume" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                <div clas="row apply">
                    <div class="col-md-12">
                        <center>
                            <input type="submit" id="submit_btn" name="submit_btn" value="Submit Form" class="apply-btn" /> <input type="reset" id="reset_btn" name="reset_btn" value="Reset Form" class="apply-btn" /></center>
                     <div class="loader" style="text-align:center;padding:15px;display:none;"><img src="http://theviewspaper.net/selectionsearch/wp-content/uploads/2017/11/giphy.gif" style="width:50px;height:50px"></div>
                    </div>
                </div>

               
            </div>
	<input type="hidden" value="<?php echo SITE_URL(); ?>/wp-admin/admin-ajax.php" id="url" />
        </form>
    </div>

</div>
<script>
     $('#applyForm').submit(function (e) {
    e.preventDefault();
    	var file = event.target.files;
       //var formData = new FormData(this);
       var data = new FormData(this);
         //console.log(data);
        data.append("action", "insert_applications");
        $.each(file, function(key, value)
    	{
      		data.append("insert_applications", value);
    	});
   //var file = formData.append('resume', document.getElementById("resume").files[0]);
    var first_name = $('#first_name').val();
    var mid_name = $('#mid_name').val();
    var last_name = $('#last_name').val();
    var male = $('#radio_male').val();
    var female = $('#radio_female').val();
    var post_applied = $('#post_applied').val();
    var dob = $('#dob').val();
    var father_name = $('#father_name').val();
    var rec_org = $('#rec_org').val();
    var rec_desig = $('#rec_desig').val();
    var mother_name = $('#mother_name').val();
    var mother_emp = $('#mother_emp').val();
    var mother_desig = $('#mother_desig').val();
    var mother_income = $('#mother_income').val();
    var status = $('#status').val();
    var spouse_name = $('#spouse_name').val();
    var spouse_emp = $('#spouse_emp').val();
    var spouse_desig = $('#spouse_desig').val();
    var spouse_income = $('#spouse_income').val();

    var name_children = $('#name_children').val();
    var sex = $('#sex').val();
    var age_children = $('#age_children').val();
    var ed_children = $('#ed_children').val();
    var school = $('#school').val();
    var res_add = $('#res_add').val();
    var comm_add = $('#comm_add').val();
    var tel = $('#tel').val();
    var email = $('#email').val();
    var mother_tongue = $('#mother_tongue').val();
    var other_lang = $('#other_lang').val();
    var exam = $('#exam').val();
    var institute = $('#institute').val();
    var subjects = $('#subjects').val();
    var from_date = $('#from_date').val();
    var board = $('#board').val();
    var to_date = $('#to_date').val();
    var marks = $('#marks').val();

    var achievements = $('#achievements').val();
    
    var comp = $('#comp').val();
    var exp_letter = $('#exp_letter').val();
    var hobby = $('#hobby').val();
    var sp_skill = $('#sp_skill').val();

    var name_location = $('#name_location').val();
    var profile = $('#profile').val();
    var salary = $('#salary').val();
    var from_duration = $('#from_duration').val();
    var board = $('#board').val();
    var reason = $('#reason').val();
    var upto = $('#upto').val();
    var subject_class = $('#subject_class').val();
    var total_exp = $('#total_exp').val();
    var classs = $('#classs').val();
    var subject = $('#subject').val();
    var workshop = $('#workshop').val();
    var teacher_train = $('#teacher_train').val();
    
    var name_ref = $('#name_ref').val();
    var occ_ref = $('#occ_ref').val();
    var add_ref = $('#add_ref').val();
    var token = $('#new_token').val();
    var url = $('#url').val();
    //var datastr = 'action=insert_applications&' + $(this).serialize();
    $(".loader").show();
    //console.log(datastr);
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        //dataType: 'json',
        processData: false,
        contentType: false,
        
        success: function (result) { 
           
           if(result=='0') {
               alert("Your form is submitted! Thank you for applying.");
               document.getElementById("applyForm").reset();
                $(".loader").hide();
                window.location = "http://localhost/wordpress/thank-you?key="+token;
           //$('#applyForm').reset();
       }
           else{alert("Please verify your form");
            $(".loader").hide();}

        }
    });


});
    jQuery("#plus_cust_brnd").click(function () {
        jQuery("#cust_brand").append('<div class="row" id="add-container"> <div class="col-md-3"><input type="text" name="name_children[]" class="form-control" value="" placeholder="Name" required/></div><div class="col-md-2"><input type="text" name="sex[]" class="form-control" value="" placeholder="Sex" required/></div><div class="col-md-2"> <input type="text" name="age_children[]" class="form-control" value="" placeholder="Age" required/></div><div class="col-md-2"> <input type="text" name="ed_children[]" class="form-control" value="" placeholder="Education" required/></div><div class="col-md-2"><input type="text" name="school[]" class="form-control" value="" placeholder="School/College" required/></div> <div class="col-md-1 "> <a href="javascript:void(0);" id="cust_brnd" class="remove_field"><i class="fa fa-minus fa-2x" aria-hidden="true"></i></a></div></div> ');
    });
    jQuery('#cust_brand').on("click", ".remove_field", function (e) {
        //user click on remove text
        e.preventDefault();
        jQuery(this).parent().parent('div').remove();
        //$(this).prev().remove(); 
    })
    jQuery("#plus_qualify").click(function () {
        jQuery("#qualify").append('<div class="row" id="add-qualify"><div class="col-md-4"><label>Examination:</label><br><select class="form-control" id="exam" name="exam[]" required><option value="Secondary">Secondary<option value="Senior Secondary"class="form-control" id="sen_sec" name="sen_sec" >Senior Secondary<option value="Graduation" class="form-control" id="grad" name="grad">Graduation<option value="Post Graduation"class="form-control" id="post_grad" name="post_grad">Post Graduation<option value="Diploma" class="form-control" id="diploma" name="diploma">Diploma<option value="B.Ed" class="form-control" id="b_ed" name="b_ed">B.Ed.<option value="M.Ed" class="form-control" id="m_ed" name="m_ed">M.Ed.<option value="Others" class=form-"control" id="other" name="other">Others</select><label>%</label><input class="form-control" id="marks" name="marks[]" placeholder=""required><br><label>Board:</label><input class="form-control" id="board" name="board[]" placeholder=""required></div><div class="col-md-4"><label>From:</label><br><input class="form-control" id="from_date" name="from_date[]" placeholder=""required type="date"><label>Institute:</label><input class="form-control" id="institute" name="institute[]" placeholder=""required><br></div><div class="col-md-4"><label>To:</label><br><input class="form-control" id="to_date" name="to_date[]" placeholder=""required type="date"><label>Subjects:</label><input class="form-control" id="subjects" name="subjects[]" placeholder=""required><br><a href="javascript:void(0);" id="minus_qualify" class="remove_field"><i aria-hidden="true" class="fa fa-2x fa-minus"></i></a></div></div>');
    });
    jQuery('#qualify').on("click", ".remove_field", function (e) {
        //user click on remove text
        e.preventDefault();
        jQuery(this).parent().parent('div').remove();
        //$(this).prev().remove(); 
    })
    jQuery("#plus_exp").click(function () {
        jQuery("#exp").append('<div class="row" id="add-exp"><div class="col-md-4"><label>Name & Location of the Institution:</label><br><textarea rows=""class="form-control" cols=""name="name_location[]"placeholder="" ></textarea><label>Job Profile:</label><input class="form-control" name="profile[]"placeholder=""><br><label>Salary Drawn:</label><input class="form-control" name="salary[]"placeholder="" ></div><div class="col-md-4"><label>From:</label><br><input type="date" class="form-control" name="from_duration[]"placeholder=""><br><label>Name of Board:</label><input class="form-control" name="name_board[]"placeholder=""><label>Reason for change:</label><input class="form-control" name="reason[]"placeholder=""></div><div class="col-md-4"><label>Upto:</label><br><input type="date" class="form-control" name="upto[]"placeholder=""><br><label>Subjects & Classes taught:</label><input class="form-control" name="subject_class[]"placeholder=""><br><a href="javascript:void(0);" id="minus_exp" class="remove_field"><i aria-hidden="true" class="fa fa-minus fa-2x"></i></a></div></div></div>');
    });
    jQuery('#exp').on("click", ".remove_field", function (e) {
        //user click on remove text
        e.preventDefault();
        jQuery(this).parent().parent('div').remove();
        //$(this).prev().remove(); 
    })
    jQuery("#plus_subject").click(function () {
        jQuery("#subject").append('<div class="row" id="add-subject"><div class="col-md-3"><input class="form-control" name="class[]"placeholder="Class" ></div><div class="col-md-3"><input class="form-control" name="subject[]"placeholder="Subject"></div><div class="col-md-3"><a href="javascript:void(0);" id="minus_subject" class="remove_field"><i aria-hidden="true" class="fa fa-minus fa-2x"></i></a></div></div></div>');
    });
    jQuery('#subject').on("click", ".remove_field", function (e) {
        //user click on remove text
        e.preventDefault();
        jQuery(this).parent().parent('div').remove();
        //$(this).prev().remove(); 
    })
    jQuery("#plus_referee").click(function () {
        jQuery("#referee").append('<div class="row" id="add-referee"><div class="col-md-3"><input class="form-control" name="name_ref[]"placeholder="Name of Referee"></div><div class="col-md-3"><input class="form-control" name="occ_ref[]"placeholder="Occupation"></div><div class="col-md-3"><input class="form-control" name="add_ref[]"placeholder="Address & Phone no."></div><div class="col-md-3"><a href="javascript:void(0);" id="plus_referee" class="remove_field"><i aria-hidden="true" class="fa fa-minus fa-2x"></i></a></div></div>');
    });
    jQuery('#referee').on("click", ".remove_field", function (e) {
        //user click on remove text
        e.preventDefault();
        jQuery(this).parent().parent('div').remove();
        //$(this).prev().remove(); 
    })
</script>
<style>
	.apply a{
	color:#222;}</style>
<script src="<?php bloginfo('template_url'); ?>/js/insert.js" type="text/javascript"></script>
<?php get_footer(); ?>