<?php get_header(); ?>
<div class="testimonials">
    <div class="jobs-banner">
        <div class="overlay-jobs">
            <div class="section-title-jobs">
                <h2 class="section-caps-jobs"><center>TESTIMONIALS</center></h2>
                <center><span class="underline-jobs"></span></center>
            </div>
        </div>
    </div>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->

        <div class="container">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php $slider = get_posts(array('post_type' => 'testimonials', 'posts_per_page' => 5)); ?>
                <?php $count = 0; ?>
                <?php foreach ($slider as $slide): ?>
                    <div class="item <?php echo ($count == 0) ? 'active' : ''; ?>">
                        <?php
                        query_posts(array('post_type' => 'testimonials'));

                        if (have_posts()) :
                            ?>
                            <div >
                                <div class="testimonial-post">
                                    <?php while (have_posts()) : the_post(); ?>
                                        
                                        <div class="test-content"><span class="quote-left"><i class="fa fa-quote-left fa-lg" aria-hidden="true"></i></span>    <p><?php the_content(); ?></p> <span class="quote-right"><i class="fa fa-quote-right fa-lg" aria-hidden="true"></i></span>  </div>
                                    <!--<p>Her
                meticulous selection brought forward an impressive short list from the best
                known schools in the country. Her careful reference checking made the selection
                thorough.</p>--> 
                                        <div class="test-title"><h3><?php the_title(); ?></h3><p><?php the_field('designation'); ?></p></div>
                                    </div>

                                <?php endwhile; ?> 
                            </div>
                        </div>
                    <?php endif;
                    wp_reset_query();
                    ?>
                    <div class="arrow-left">
                        <a class="left" href="#myCarousel" data-slide="prev">
                            <span><i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </div>
                    <div class="arrow-right">
                        <a class="right" href="#myCarousel" data-slide="next">
                            <span><i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </div>  
                </div>
                <?php $count++; ?>
<?php endforeach; ?>
        </div>

    </div>


</div>

<?php get_footer(); ?>