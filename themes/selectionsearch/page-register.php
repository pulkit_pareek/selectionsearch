<?php
get_header();?>

<div class="register_page">
    <div class="section welcome-section-new">
    <div class="overlay-register">
        
    <div class="container">
        <div class="section-title3">
            <h2 class="section-caps-register"><center>WELCOME TO SELECTION SEARCH</center></h2>
            <center><span class="underline3"></span></center>
        </div>
        <div class="welcome-register">

            <p>Driven by an earnest desire to facilitate job opportunities to deserving individuals, and counsel and address the issue relating to careers and required skill sets, Selection Search was formed in March, 2006.
                Spearheaded by a team of individuals who have had valuable experience in the field of education for more than a decade Selection Search today caters to requirements fort diverse industries.</p>
            <p>It is one of the few search companies which is also facilitating educational institutions throughout the country in finding competent and capable leaders and educators.</p>
        </div>
    </div>
</div>
    </div>
    <div class="section2 register-form">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title3">
            <h2 class="section-caps-register"><center>REGISTER</center></h2>
            <center><span class="underline3"></span></center>
        </div>
            </div>
        </div>
       <div class="section apply-now">
    <div class="container">
        <form method="post" id="registerForm">
            
               
                <!--div class="row apply">
                    <div class="col-md-3">
                        <label>Name:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="first_name" name="first_name" class="form-control" value="" placeholder="First Name" required />
                    </div>
                    
                    <div class="col-md-3">
                        <input type="text" id="last_name" name="last_name" class="form-control" value="" placeholder="Last Name" />
                    </div>
                </div>
               
                <div class="row apply">
                    <div class="col-md-3">
                        <label>Telephone No.:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="tel" name="tel" class="form-control" value="" placeholder="" required />
                    </div>
                </div>
                    <div class="row apply">
                    <div class="col-md-3">
                        <label>Email ID:</label>
                    </div>
                    <div class="col-md-3">
                        <input type="email" id="email" name="email" class="form-control" value="" placeholder="" required />
                    </div>
                </div-->
                
                <div clas="row apply">
                    <div class="col-md-12">
                        <center>
                            <input type="submit" id="submit_btn" name="submit_btn" value="Send Emails" class="apply-btn" /> <!--input type="reset" id="reset_btn" name="reset_btn" value="Reset Form" class="apply-btn" /--></center>
                     <div class="loader" style="text-align:center;padding:15px;display:none;"><img src="http://theviewspaper.net/selectionsearch/wp-content/uploads/2017/11/giphy.gif" style="width:50px;height:50px"></div>
                    </div>
                </div>

               
            
	<input type="hidden" value="<?php echo SITE_URL(); ?>/wp-admin/admin-ajax.php" id="url" />
        <input type="hidden" name="token" id="random_token">
        </form>
        </div>
    </div>
    </div>
</div>
    <script>
    function random_token() {
  var min = 1;
  var max = 9999999999;
  var num = Math.floor(Math.random() * (max - min + 1)) + min;
  var timeNow = new Date().getTime();
  document.getElementById('random_token').value = num + '_' + timeNow;
}
window.onload = random_token;
$('#registerForm').submit(function (e) {
    e.preventDefault();
   // alert("test");
    	var file = event.target.files;
       //var formData = new FormData(this);
       var data = new FormData(this);
         //console.log(data);
        data.append("action", "insert_register_applications");
        $.each(file, function(key, value)
    	{
      		data.append("insert_register_applications", value);
    	});
   
    var first_name = $('#first_name').val();
    var last_name = $('#last_name').val();
    var tel = $('#tel').val();
    var email = $('#email').val();
    var token = $('#random_token').val();
    
    var url = $('#url').val();
    //var datastr = 'action=insert_applications&' + $(this).serialize();
    $(".loader").show();
    //console.log(datastr);
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        //dataType: 'json',
        processData: false,
        contentType: false,
        
        success: function (result) { 
           //alert(result);
           if(result=='0') {
               alert("All the mails are sent.");
               document.getElementById("registerForm").reset();
               location.reload();
                $(".loader").hide();
           
       }
           else{alert("Please verify your form");
            $(".loader").hide();}

        }
    });


});


    </script>
    
    
    </div>
<?php get_footer(); ?>


