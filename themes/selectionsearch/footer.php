<?php wp_footer();?> 
<div class="footer-top">
        <!--Footer-->
        <div class="container">
            <div class="footer-up">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="news-title">
                            <h4>Subscribe To Our Newsletter</h4>  
                        </div>
                        <div class="news-box">
						<?php echo do_shortcode('[mc4wp_form id="133"]'); ?>                            
<!--input type="text" class="newsletter" placeholder="Enter your Email Address" ><button type="submit" class="news-btn"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>
SUBSCRIBE</button-->  
                        </div>
                        <!--<ul class="news">
                            <li class="news-letter-title">Subscribe Our Newsletter</li>
                            <li class="news-letter"><input type="text" class="newsletter" placeholder="Enter your Email Address" ><button type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>
SUBSCRIBE</button></li>
                        </ul>-->
                    </div>
                    <div class="col-sm-3">
                        <div class="social-title">
                            <h4>Follow us on </h4>  
                        </div>
                        <ul class="social-footer">
                           
                        <li><a href="#" class="pinterest"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>    
                        <li><a href="#" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        
                        

                    </ul>

                    </div>
                    <div class="col-sm-4">
                        <div class="social-title">
                            <h4>Need Help? </h4>  
                            <div class="help-footer">
                                <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                               <span class="number">+91 7291987087</span>
                                <!--<ul class="phone">
                                    <li><i class="fa fa-phone fa-2x" aria-hidden="true"></i></li>
                                    <li><span class="">CALL US:</span></li>
                                </ul>-->
                        </div>
                </div>
            </div>
            </div>
            </div>
            <div class="footer-content">
                
                <div class="row">
                    <div class="col-sm-5">
                        
                        <img src="<?php bloginfo('template_url')?>/images/logo-inverse.png" alt="image" class="foot-img">
                        
                    </div>
                    <div class="col-sm-3">
                        <div class="foot-title">
                            <h4>Information</h4></div>
                        <ul class="foot-links">
                            <li><a href="http://selectionsearch.in"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;Home Page</a></li>
                            <li><a href="http://selectionsearch.in/about"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;About Us</a></li>
                            <li><a href="http://selectionsearch.in/jobs"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;Jobs</a></li>
							<li><a href="http://selectionsearch.in/register-now"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;Register Now</a></li>
                            <!--li><a href="#"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;Privacy Policy</a></li>
                            <li><a href="#"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;Terms & Conditions</a></li-->
                        </ul>
                    </div>
                    <!--div class="col-sm-3">
                        <div class="blog-post">
                            <div class="foot-title">
                                <h4>Useful Links</h4></div>
                            <ul class="useful-links">
                            <li><a href="#"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;Home Page</a></li>
                            <li><a href="#"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;Submit Job</a></li>
                            <li><a href="#"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;All Candidates</a></li>
                            <li><a href="#"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;Latest Blogs</a></li>
                            <li><a href="#"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>&nbsp;Jobs</a></li>
                        </ul>
                        </div>
                    </div-->
                    <div class="col-sm-3">
                        <div class="foot-title">
                            <h4>Get in touch</h4></div>
                        <div class="box">
                            <?php echo do_shortcode('[contact-form-7 id="112" title="Contact form 1"]'); ?>
                        <!--div class="row">
                            <div class="col-sm-12">

                                <input type="text" class="contact-box" placeholder="Name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">

                                <input type="text" class="contact-box" placeholder="Email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                
                                <input type="text" class="textarr" placeholder="Message"><button type="submit" class="message-btn"><i class="fa fa-paper-plane-o fa-lg" aria-hidden="true"></i></button>
                                
                                </div>
                        </div-->
                        </div>    
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    Copyright &copy; 2017. All rights Reserved
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
 		<script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 4,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
				responsiveClass:true,
   					 responsive:{
        		0:{
            items:2
            
        },
        600:{
            items:3
            
        },
		1000:{
            items:4
            
        }
      
    }
                
              });
            
                owl.trigger('play.owl.autoplay', [2000]);
            })
            
            
            <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109960197-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109960197-1');
</script>

 <style>
  .img_dash img {
    width: 100%;
    height: 140px;
    object-fit: cover;
}
@media only screen and (max-width: 500px) {
    .logo {
   
    height: 80px !important;
    width: 206px !important;
}
}
.grid_jobs {
    margin-bottom: 30px;
    min-height: 400px;
}

</style>
<style>
        .text-cat{
                text-align: center;
    margin: 20px 0px;
}
.text-cat li {
        padding: 10px;
        background: #000;
        color: #fff;
        margin-right: 15px;
        cursor: pointer;
        border-radius: 5px;
        text-transform: uppercase;
}
.text-cat li:hover {
        color: #FF8E05;
}
.text-cat li.active {
       color: #FF8E05;
}
    </style>
    <script>
    function menu1() {
        $('#menu_1').addClass('active');
        $('#menu_2').removeClass('active');
        $('#menu_3').removeClass('active');
        $('#lead_1').show();
        $('#lead_2').hide();
        $('#lead_3').hide();
       

    }
    function menu2() {
        $('#menu_2').addClass('active');
        $('#menu_1').removeClass('active');
        $('#menu_3').removeClass('active');
       
         $('#lead_2').show();
        $('#lead_1').hide();
        $('#lead_3').hide();


    }
    function menu3() {
        $('#menu_3').addClass('active');
        $('#menu_2').removeClass('active');
        $('#menu_1').removeClass('active');
        
         $('#lead_3').show();
        $('#lead_2').hide();
        $('#lead_1').hide();

    }
 

</script>
</body>

</html>