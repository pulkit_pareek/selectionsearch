<?php get_header(); ?>
<div class="jobs-banner">
               <div class="overlay-jobs">
            <div class="section-title-jobs">
                <h2 class="section-caps-jobs"><center>Contact Us</center></h2>
                <center><span class="underline-jobs"></span></center>
            </div>
               </div>
            </div>
<div class="container">
    <div class="section contact-us">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="contact-title">
                    <h2>Get in touch</h2>
                    <div class="contact-add">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<span>A-3/19, Block A,
                            Sector 26A, DLF Phase -1,
                            Gurugram, Haryana - 122002</span>
                    </div> 
                    <div class="contact-add">
                        <i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;<span>+91 7291987087</span><br />&nbsp;&nbsp;&nbsp;&nbsp;<span>+91 7291987082</span>
                    </div> 
                    <div class="contact-add">
                        <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;<span>anuradha@selectionsearch.in</span>
                    </div> 
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="contact-title">
                    <h2>Leave a Message</h2>
                </div> 
                <div class="contact-form">
                    <div class="form-group">
                        <input class="form-control" id="contact-name" type="text" placeholder="Name">
                        <input class="form-control" id="contact-num" type="text" placeholder="Contact Number">
                        <textarea class="form-control" id="contact-message" rows="3" placeholder="Message"></textarea>
                        <button class="btn btn-warning">SEND</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>