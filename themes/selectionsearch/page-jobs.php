<?php get_header(); ?>
<div class="jobs-page">
        

           <div class="jobs-banner">
               <div class="overlay-jobs">
            <div class="section-title-jobs">
                <h2 class="section-caps-jobs"><center>JOBS</center></h2>
                <center><span class="underline-jobs"></span></center>
            </div>
               </div>
            </div>
<div class="section section-featured">
    <div class="container">
        <div class="main-body">

            <div class="row grids">
                <?php
                query_posts(array('post_type' => 'jobs', 'posts_per_page' => 16));

                if (have_posts()) :
                    ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-sm-3">

                            <div class="grid_jobs">
                                <div class="img-responsive img_dash">
                                    <?php
                                     $job = get_field('job_location');
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail();
                                    }else { ?>
                          <img src="http://selectionsearch.in/wp-content/uploads/2017/12/default_img.png" > 
                                   <?php }  ?>
                                    <!--<img src="" alt="image" class="img-responsive">--></div>
                                <div class="featured-box">
                                    <div class="featured-title">
                                        <span class="featured-box-title"><?php the_title(); ?></span></div>
                                    <div class="bookmark"><?php if($job != "") { ?><span><i class="fa fa-map-marker" aria-hidden="true"></i></span><span class="job-location"><?php the_field('job_location'); ?></span> <?php } ?>
                                    </div>
                                    <div class="featured-box-desc">
                                        <p><?php the_field('short_description'); ?></p>
                                    </div>
                                    <div class="featured-box-btn">
                                        <div class="list-inline listing_jobs"><div class="jobs_btn"><a href="<?php the_permalink(); ?>">VIEW MORE</a></div><div class="jobs_btn"><a href="<?php echo SITE_URL(); ?>/apply?id=<?php the_field('job_id'); ?>">APPLY NOW</a></div></div>
                                    </div>
                                    <!--div class="featured-box-btn">
                                        <button class="featured-btn1">VIEW MORE</button>
                                        <button class="featured-btn2"onclick="location.href = 'http://localhost/wordpress/apply'">APPLY NOW</button>
                                    </div-->
                                </div>
                            </div>

                        </div>
                    <?php endwhile; ?> 
                <?php
                endif;
                wp_reset_query();
                ?> 
                
            </div>


        </div>

    </div>

</div>
</div>
<?php get_footer(); ?>